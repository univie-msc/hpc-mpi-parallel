#include "pch.h"
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>

/**
 * For the solution of this parallel implementation I used the following example as reference:
 * Simple Heat Equation at https://computing.llnl.gov/tutorials/parallel_comp/
 */

#define MASTER 0
#define NO_NEIGHBOUR -1
#define TAG_BEGIN 1
#define TAG_BOTTOM_T 2
#define TAG_TOP_V 3
 // S not needed as matrix is decomposed horizontally

int width, height, nIter;
float outOfBoundVal = 1.0e-6;
float **S, **V, **T, **M, **D, **L;
float *sData, *tData, *vData, *mData, *dData, *lData; // helpers for memory allocation
double startTime, endTime;

void initialize(int, int);
void updateST(int, int);
void updateV(int, int);
float sum(int, int);
float max(int, int);
void createMatrix();
void freeMatrix();
void printMatrix(float**, int, int);

int main(int argc, char *argv[])
{
	int rank, nWorkers, avgRow, extraRows;
	int dest, i;

	int top, bottom, nRows, rowOffset, tmpTop, tmpBottom, tmpNRows, tmpRowOffset;

	int rowStart, rowEnd, pulse_x, pulse_y, pulse_counter;
	float t;

	float allSum, allMax, localSum = 0.0f, localMax = 0.0f;

	MPI_Status status;
	MPI_Status statusT;
	MPI_Status statusV;
	MPI_Request requestT;
	MPI_Request requestV;

	if (argc != 4) {
		printf("Arguments missing.\narg1=width arg2=height arg3=nIter\n");
		return 1;
	}

	width = atoi(argv[1]);
	height = atoi(argv[2]);
	nIter = atoi(argv[3]);

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &nWorkers);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	startTime = MPI_Wtime();

	createMatrix();

	/********** MASTER task **********/
	if (rank == MASTER) {
		avgRow = height / nWorkers;
		extraRows = height % nWorkers;

		rowOffset = 0;

		// allocate work
		for (dest = 0; dest < nWorkers; dest++) {
			// calculate # of rows for each worker - extras may be assigned to first ones 
			nRows = (dest < extraRows) ? avgRow + 1 : avgRow;

			// top neighbour
			if (dest == 0)
				top = NO_NEIGHBOUR;
			else
				top = dest - 1;
			// bottom neighbour
			if (dest == nWorkers - 1)
				bottom = NO_NEIGHBOUR;
			else
				bottom = dest + 1;

			if (dest > MASTER) {
				// send initial data
				MPI_Send(&top, 1, MPI_INT, dest, TAG_BEGIN, MPI_COMM_WORLD);
				MPI_Send(&bottom, 1, MPI_INT, dest, TAG_BEGIN, MPI_COMM_WORLD);
				MPI_Send(&nRows, 1, MPI_INT, dest, TAG_BEGIN, MPI_COMM_WORLD);
				MPI_Send(&rowOffset, 1, MPI_INT, dest, TAG_BEGIN, MPI_COMM_WORLD);
			} else {
				// temporary storage for MASTER
				tmpTop = top;
				tmpBottom = bottom;
				tmpNRows = nRows;
				tmpRowOffset = rowOffset;
			}

			rowOffset += nRows;
		}

		top = tmpTop;
		bottom = tmpBottom;
		nRows = tmpNRows;
		rowOffset = tmpRowOffset;
	}

	/********** WORKER task **********/
	if (rank > MASTER) {
		MPI_Recv(&top, 1, MPI_INT, MASTER, TAG_BEGIN, MPI_COMM_WORLD, &status);
		MPI_Recv(&bottom, 1, MPI_INT, MASTER, TAG_BEGIN, MPI_COMM_WORLD, &status);
		MPI_Recv(&nRows, 1, MPI_INT, MASTER, TAG_BEGIN, MPI_COMM_WORLD, &status);
		MPI_Recv(&rowOffset, 1, MPI_INT, MASTER, TAG_BEGIN, MPI_COMM_WORLD, &status);
	}

	rowStart = rowOffset;
	rowEnd = rowOffset + nRows - 1;

	//printf("rank=%d top=%d bottom=%d nRows=%d rowStart=%d rowEnd=%d\n", rank, top, bottom, nRows, rowStart, rowEnd);

	initialize(rowStart, rowEnd);

	pulse_x = width / 3;
	pulse_y = height / 4;
	pulse_counter = 10;

	for (i = 0; i < nIter; i++) {
		// initial pulse
		if (i < 10 && rowStart <= pulse_y && rowEnd >= pulse_y) {
			t = (pulse_counter - i) * 0.05f;
			V[pulse_y][pulse_x] += 64 * sqrt(M[pulse_y][pulse_x]) * exp(-t * t);
		}

		if (top != NO_NEIGHBOUR) {
			MPI_Isend(&V[rowStart][0], width, MPI_FLOAT, top, TAG_TOP_V, MPI_COMM_WORLD, &requestV);
		}

		if (bottom != NO_NEIGHBOUR) {
			MPI_Recv(&V[rowEnd + 1][0], width, MPI_FLOAT, bottom, TAG_TOP_V, MPI_COMM_WORLD, &statusV);
		}

		if (top != NO_NEIGHBOUR) {
			MPI_Wait(&requestV, &statusV);
		}

		updateST(rowStart, rowEnd);

		if (bottom != NO_NEIGHBOUR) {
			MPI_Isend(&T[rowEnd][0], width, MPI_FLOAT, bottom, TAG_BOTTOM_T, MPI_COMM_WORLD, &requestT);
		}

		if (top != NO_NEIGHBOUR) {
			MPI_Recv(&T[rowStart - 1][0], width, MPI_FLOAT, top, TAG_BOTTOM_T, MPI_COMM_WORLD, &statusT);
		}

		if (bottom != NO_NEIGHBOUR) {
			MPI_Wait(&requestT, &statusT);
		}

		updateV(rowStart, rowEnd);

		// last iteration: calculate partial (local) values
		if (i == nIter - 1) {
			localSum = sum(rowOffset, rowOffset + nRows - 1);
			localMax = max(rowOffset, rowOffset + nRows - 1);

			MPI_Reduce(&localSum, &allSum, 1, MPI_FLOAT, MPI_SUM, MASTER, MPI_COMM_WORLD);
			MPI_Reduce(&localMax, &allMax, 1, MPI_FLOAT, MPI_MAX, MASTER, MPI_COMM_WORLD);
		}
	}

	/********** MASTER task **********/
	if (rank == MASTER) {
		endTime = MPI_Wtime();

		printf("MAX=%f AVG=%f SUM=%f elapsed time=%lf\n", allMax, allSum / (width * height), allSum, endTime - startTime);
	}

	freeMatrix();

	MPI_Finalize();

	return 0;
}

void initialize(int rowStart, int rowEnd)
{
	int i, j;
	int start, end; // helpers for M,L

	start = (rowStart > height / 2) ? rowStart : (height / 2);
	end = (rowEnd < (height / 2 - 1)) ? rowEnd : (height / 2 - 1);

	// initialize V,S,T
	for (i = rowStart; i <= rowEnd; i++)
		for (j = 0; j < width; j++)
			V[i][j] = S[i][j] = T[i][j] = 1.0e-6;

	// initialize D
	for (i = rowStart; i <= rowEnd; i++)
		for (j = 0; j < width; j++) {
			if (i == 0 || i == height - 1 ||
				j == 0 || j == width - 1)
				D[i][j] = 0.6f;
			else if (i == 1 || i == height - 2 ||
				j == 1 || j == width - 2)
				D[i][j] = 0.7f;
			else if (i == 2 || i == height - 3 ||
				j == 2 || j == width - 3)
				D[i][j] = 0.8f;
			else if (i == 3 || i == height - 4 ||
				j == 3 || j == width - 4)
				D[i][j] = 0.9f;
			else
				D[i][j] = 1.0f;
		}

	// initialize M,L
	for (i = rowStart; i <= end; i++)
		for (j = 0; j < width; j++)
			M[i][j] = L[i][j] = 0.125f;
	for (i = start; i <= rowEnd; i++)
		for (j = 0; j < width; j++) {
			M[i][j] = 0.3f;
			L[i][j] = 0.4f;
		}
}

void updateST(int rowStart, int rowEnd)
{
	int i, j;
	float vVal;

	for (i = rowStart; i <= rowEnd; i++) {
		for (j = 0; j < width; j++) {
			vVal = (i < height - 1) ? V[i + 1][j] : outOfBoundVal;
			T[i][j] = T[i][j] + M[i][j] * (vVal - V[i][j]);

			vVal = (j + 1 < width) ? V[i][j + 1] : outOfBoundVal;
			S[i][j] = S[i][j] + M[i][j] * (vVal - V[i][j]);
		}
	}
}

void updateV(int rowStart, int rowEnd)
{
	int i, j;
	float vVal, sVal, tVal;

	for (i = rowStart; i <= rowEnd; i++) {
		for (j = 0; j < width; j++) {
			sVal = (j >= 1) ? S[i][j - 1] : outOfBoundVal;
			tVal = (i >= 1) ? T[i - 1][j] : outOfBoundVal;

			V[i][j] = D[i][j] * (V[i][j] + L[i][j] * (S[i][j] - sVal + T[i][j] - tVal));
		}
	}
}

float sum(int rowStart, int rowEnd)
{
	int i, j;
	float sumVal = 0.0f;

	for (i = rowStart; i <= rowEnd; i++) {
		for (j = 0; j < width; j++) {
			sumVal += V[i][j];
		}
	}

	return sumVal;
}

float max(int rowStart, int rowEnd)
{
	int i, j;
	float maxVal = FLT_MIN;

	for (i = rowStart; i <= rowEnd; i++) {
		for (j = 0; j < width; j++) {
			if (V[i][j] > maxVal)
				maxVal = V[i][j];
		}
	}

	return maxVal;
}

void createMatrix()
{
	int i;

	sData = (float*)malloc(height * width * sizeof(float));
	S = (float**)malloc(height * sizeof(float*));
	for (i = 0; i < height; i++)
		S[i] = sData + width * i;

	tData = (float*)malloc(height * width * sizeof(float));
	T = (float**)malloc(height * sizeof(float*));
	for (i = 0; i < height; i++)
		T[i] = tData + width * i;

	vData = (float*)malloc(height * width * sizeof(float));
	V = (float**)malloc(height * sizeof(float*));
	for (i = 0; i < height; i++)
		V[i] = vData + width * i;

	mData = (float*)malloc(height * width * sizeof(float));
	M = (float**)malloc(height * sizeof(float*));
	for (i = 0; i < height; i++)
		M[i] = mData + width * i;

	dData = (float*)malloc(height * width * sizeof(float));
	D = (float**)malloc(height * sizeof(float*));
	for (i = 0; i < height; i++)
		D[i] = dData + width * i;

	lData = (float*)malloc(height * width * sizeof(float));
	L = (float**)malloc(height * sizeof(float*));
	for (i = 0; i < height; i++)
		L[i] = lData + width * i;
}

void freeMatrix()
{
	free(sData);
	free(vData);
	free(tData);
	free(mData);
	free(dData);
	free(lData);

	free(S);
	free(V);
	free(T);
	free(M);
	free(D);
	free(L);
}

void printMatrix(float** matrix, int rowStart, int rowEnd)
{
	int i, j;

	for (i = rowStart; i <= rowEnd; i++) {
		for (j = 0; j < width; j++)
			printf("%f ", matrix[i][j]);
		printf("\n");
	}
	printf("\n");
}